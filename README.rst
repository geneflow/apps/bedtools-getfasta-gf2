BedTools GetFasta GeneFlow App
==============================

Version: 2.29.2-01

This GeneFlow app wraps the bedtools getfasta tool.

Inputs
------

1. input: Input FASTA file with masked sites.

2. bedfile: Reference GFF file.

Parameters
----------

1. output: Output FASTA file.
